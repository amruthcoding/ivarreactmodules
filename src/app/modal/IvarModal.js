import React, { Component } from 'react';
import Modal from 'react-modal';


class IvarModal extends Component {

    constructor() {
        super();
        this.state = {modalIsOpen:false};
    }
    open() {
        this.state.modalIsOpen = true;
    }
    close() {
        this.state.modalIsOpen = false;
    }

    render() {
        return (
            <Modal
                isOpen={this.state.modalIsOpen}
                onRequestClose={this.close}
                contentLabel="Modal">
            </Modal>
        )
    }
}

export default IvarModal;
