import React, { Component } from 'react';
import { CardStack, Card } from 'react-cardstack';
import IVarItem  from './IVarItem';
import RECIPES from './pins/ivarlist-mock';


const styles = {
    title: {
        fontSize: '1.2em'
    }
}
class IVarList extends Component {

    index = 0;

    handleCardClick(idx) {
        this.index = 0;
        this.setState({
            index : idx
        })

    }
    cardClicked() {
        this.setState({
            index:this.index
        })
    }
    render() {
        var items = [];
        for(var i=0; i <RECIPES.length;++i) {
           items.push( <Card id={RECIPES[i].id} background='#2980B9' cardClicked={this.handleCardClick.bind(this,i)}>
                            <span style={styles.title}>{RECIPES[i].name}</span>
                        </Card>
           );
        }


     return(
         <div>
          <CardStack
            height={500}
            width={400}
            background='#f8f8f8'
            hoverOffset={25}>
            {items}

        </CardStack>
             <div>
                 <IVarItem item={this.state}  />
             </div>

         </div>
      );
    }
}

export default IVarList;