/**
 * Created by Narer on 1/21/2017.
 */


import React, { Component } from 'react';
import RECIPES from './ivarlist-mock';
import Modal from 'react-modal';


const styles = {
    pin : {
        border: '1px solid #ddd',
        border: '1px solid rgb(221, 221, 221)',
        margin: '10px',
        padding: '10px',
        cursor:'pointer'
    },
    image : {
        borderRadius: '50%'
    },
    content: {
      padding : '5px'
    },
    imgDiv : {
        width: '40%',
    },
    desDiv : {
        textAlign:'left',
        title : {
            fontWeight:'bold'
        }
    }


};

class IVarPins extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalOpen : false
        };
        this.showDetails = this.showDetails.bind(this);
    }

    showDetails(i)  {
       // this.createModalContent(RECIPES[i]);
        this.setState({modalOpen:true});
    }

    closeModal = () => {
        this.setState({modalOpen:false});
    }

    createModalContent = (item) => {
        this.modelContent = [];
        console.log(item);

    }

    render() {
        var items = [];

        for(var i=0; i <RECIPES.length;++i) {
            items.push(
                <div style={styles.pin} id={RECIPES[i].id} onClick={this.showDetails(i)}>
                    <div style={styles.imgDiv}>
                        <img  style={styles.image} src={RECIPES[i].imageUrl}/>
                    </div>
                    <div style={styles.content}>
                        <div style={styles.desDiv}>
                            <div style={styles.desDiv.title}>{RECIPES[i].name}</div>
                            <div style={styles.desDiv.description}>{RECIPES[i].description}</div>
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div class="app1">
                <div> this is list app</div>
                <Modal isOpen={this.state.modalOpen} onRequestClose={this.closeModal}  contentLabel='Modal'>
                    {this.modalContent}
                </Modal>
                 <div class='pins'>
                    {items}
                 </div>
            </div>
        );
    }
}
export default IVarPins;
