/**
 * Created by narer on 1/12/2017.
 */
import React, { Component } from 'react';
import { CardStack, Card } from 'react-cardstack';
import RECIPES from './pins/ivarlist-mock';

class IVarItem extends Component {
    showDetails = true;

    handleClick() {
        console.log('on item clicked');
        this.setState({
            showDetails : false
        });
        this.props.cardClicked();
    }
    render() {
        console.log(this.props);
        this.showDetails = !this.showDetails;
        console.log(this.showDetails);
        if(this.props.item) {
            var item = RECIPES[this.props.item.index];
        }
        console.log(item);
        return ( this.showDetails ?
                <div style={{ position: 'absolute',top:15 }} >
                    <div>
                        <h1> details </h1>
                         <div> <img src={item.imageUrl}/></div>
                        <div> {item.description}</div>
                 </div>
                </div>
                : <div></div>
        );
    }
}

export default IVarItem;