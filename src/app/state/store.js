/**
 * Created by narer on 2/16/2017.
 */

import {createStore} from 'redux';
import  stateReducer  from './reducers';


let store = createStore(stateReducer);

export default store;