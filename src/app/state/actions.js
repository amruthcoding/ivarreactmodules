/**
 * Created by narer on 2/16/2017.
 */


export const LIST_VIEW = 'LIST_VIEW';
export const GRID_VIEW = 'GRID_VIEW';


export function listView() {
    return {id : LIST_VIEW};
}

export function gridView() {
    return {id : GRID_VIEW };
}

