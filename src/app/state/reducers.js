/**
 * Created by narer on 2/16/2017.
 */


import {listView,gridView} from './actions';

const initialState = {
    viewType : listView
}


 var stateReducer = function(state = initialState,viewType) {

    switch(viewType) {
        case listView :
            return state;
        case gridView : {
            return Object.assign({},state, {viewType:gridView})
        }
        default:
            return state;
    }
}

export default stateReducer;