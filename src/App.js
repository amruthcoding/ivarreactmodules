import React, { Component } from 'react';
import IVarPins from './app/pins/IVarPins';
import './App.css';
import { connect } from 'react-redux'




class App extends Component {
    constructor(props) {
        super(props);
        this.selectView = this.selectView.bind(this);
    }

    selectView(e) {
        console.log(e);
        console.log(this.state);
    }

  render() {
    return (
      <div className="App">
          <div className="actions">
              <a href='#' onClick={this.selectView}>List View</a>
              <a href='#' onClick={this.selectView}>Pin View </a>
          </div>
          <IVarPins />
      </div>
    );
  };
}

let mapStateToProps = (state, ownProps) => {
    console.log('At State');
    console.log(state);
    return state;
}

let mapDispatchToProps = (dispatch, ownProps) => {
    return dispatch;
}

export default App;
